# Vaccination Find

How to use?
Pre-requisits:

Required python version 3 and above

How to run:

Run the `findVaccineSlot` exe file inside exe folder by passing three arguments. <br>

<b>First argument</b> : age 18 or 45 <br>
<b>Second argument</b>: date in dd-mm-yyyy format <br>
<b>Third argument</b>: Disctrict ID or PINCODE <br>


<br> example for disctrict search </b>: `./exe/findVaccineSlot 18 12-05-2021 265`
<br> example for pincode search </b>: `./exe/findVaccineSlot 18 12-05-2021 560010`
<br>
<br>
<b>Find the disctrict IDs </b>:  <br>
264 - Belgaum<br>
265 - Bangalore Urban<br>
266 - Mysore<br>
267 - Gulbarga<br>
268 - Chitradurga<br>
269 - Dakshina Kannada<br>
270 - Bagalkot<br>
271 - Chamarajanagar<br>
272 - Bidar<br>
273 - Chikamagalur<br>
274 - Bellary <br>
275 - Davangere<br>
276 - Bangalore Rural<br>
277 - Kolar<br>
278 - Dharwad<br>
279 - Haveri<br>
280 - Gadag<br>
281 - Uttar kannada<br>
282 - Koppal<br>
283 - Kodagu<br>
284 - Raichur<br>
285 - Yadagir<br>
286 - Udupi<br>
287 - Shimoga<br>
288 - Tumkur<br>
289 - Hassan<br>
290 - Mandya<br>
291 - Chikkabalapura<br>
292 - Ramangara<br>
293 - Vijayapura <br>
294 - BBMP<br>

-Avinash Bhat

