import requests
import os
import math
import sys
import sched, time
from requests.auth import HTTPBasicAuth

s = sched.scheduler(time.time, time.sleep)
n = len(sys.argv)
if n <= 3:
    print("Exiting : Provide correct parameters")
    print("First argument age: 18 or 45")
    print("Second argument date in format: DD-MM-YYYY. example: 12-05-2021")
    print("Third argument districtId or Pincode, find the district ID in Readme file")
    print("Example for District Id:  findVaccineSlot 18 12-05-2021 265")
    print("Example for Pincode: findVaccineSlot 18 12-05-2021 560066")
    sys.exit()


def count_digits(number):
    n = str(number)
    return len(n)


def do_something(sc):
  try:
    pincode = True
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    if count_digits(sys.argv[3]) != 6:
        pincode = False
        receive = requests.get('https://cdn-api.co-vin.in/api/v2/appointment/sessions/calendarByDistrict?district_id='+sys.argv[3]+'&date='+sys.argv[2], headers=headers, auth=HTTPBasicAuth('user', 'pass'))
    else:
        receive = requests.get('https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/findByPin?pincode='+sys.argv[3]+'&date='+sys.argv[2], headers=headers, auth=HTTPBasicAuth('user', 'pass'))

    r = receive.json()
    if pincode:
        for session in r['sessions']:
            if session['min_age_limit'] == int(sys.argv[1]) and session['available_capacity'] != 0:
                print("=======================================")
                print(session['pincode'])
                print(session['name'])
                print(session['available_capacity'])
                os.system('say "Vaccine Available"')
                print("=======================================")
        s.enter(1, 1, do_something, (s,))
    else:
        for center in r['centers']:
            for session in center['sessions']:
                if session['min_age_limit'] == int(sys.argv[1]) and session['available_capacity'] != 0:
                    print("=======================================")
                    print(center['pincode'])
                    print(center['name'])
                    print(session['available_capacity'])
                    os.system('say "Vaccine Available"')
                    print("=======================================")
        s.enter(1, 1, do_something, (s,))
  except:
    print("Not available!!! Trying again.")
    s.enter(1, 1, do_something, (s,))

s.enter(1, 1, do_something, (s,))
s.run()
